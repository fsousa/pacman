/**
 * Copyright (C) 1997-2010 Junyang Gu <mikejyg@gmail.com>
 * 
 * This file is part of javaiPacman.
 *
 * javaiPacman is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * javaiPacman is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with javaiPacman.  If not, see <http://www.gnu.org/licenses/>.
 */

package mikejyg.javaipacman.pacman;

import java.lang.Error;
import java.util.Stack;
import java.awt.*;


public class Ghost
{
    static public final int IN=0;
	static public final int OUT=1;
	static public final int BLIND=2;
	static public final int EYE=3;
	
	// AI CONSTANSTS, CHANGE HERE TO CHANGE PERFORMANCE
	static public final int HEURISTIC_TYPE = 4;
	static public final int STORE_DIRECTIONS = 4;
	static public final boolean GOT_IA = false;
	
	static public final int[] steps=	{7, 7, 1, 1};
	static public final int[] frames=	{8, 8, 2, 1};
	Stack<Integer> directions = new Stack<Integer>();
	
	static public final int INIT_BLIND_COUNT=600;	// remain blind for ??? frames
	int blindCount;
	final SpeedControl speedControl = new SpeedControl();

	int iX, iY, iDir, iStatus;
	int iBlink, iBlindCount;

	// random calculation factors
	static public final int DIR_FACTOR=2;
	static public final int POS_FACTOR=10;

	public int getMod(int a) {
		if (a < 0) return -a;
		return a;
	}
	public int max(int a, int b) {
		if (a > b) return a;
		return b;
	}
	
	public int getHeuristic(int x, int y, int destX, int destY) {
		switch(HEURISTIC_TYPE) {
		case 0:	
			return 0; // NO HEURISTIC, ONLY DIJKSTRA
		case 1:
			return getMod(x-y) + getMod(destX-destY); // Manhattan
		case 2:
			return (destX-x)*(destX-x) + (destY-y)*(destY-y); // Euclidiana
		case 3:
			return max(getMod(destX-x), getMod(y-destY)); // Chebyshev
		case 4: 
			return max((getMod(x-y) + getMod(destX-destY)),max(((destX-x)*(destX-x) + (destY-y)*(destY-y)),(max(getMod(destX-x), getMod(y-destY))))); 
		}
		
		return 0;
	}
	

	// the applet this object is associated to
	Window applet;
	Graphics graphics;

	// the maze the ghosts knows
	Maze maze;

	// the ghost image
	Image imageGhost; 
	Image imageBlind;
	Image imageEye;

	Ghost(Window a, Graphics g, Maze m, Color color)
	{
		applet=a;
		graphics=g;
		maze=m;

		imageGhost=applet.createImage(18,18);
		Images.drawGhost(imageGhost, 0, color);

		imageBlind=applet.createImage(18,18);
		Images.drawGhost(imageBlind,1, Color.white);

		imageEye=applet.createImage(18,18);
		Images.drawGhost(imageEye,2, Color.lightGray);
	}

	public void start(int initialPosition, int round)
	{
		if (initialPosition>=2)
			initialPosition++;
		iX=(8+initialPosition)*16; iY=8*16;
		iDir=3;
		iStatus=IN;

		blindCount=INIT_BLIND_COUNT/((round+1)/2);

		speedControl.start(steps[iStatus], frames[iStatus]);
	}

	public void draw()
	{
		maze.drawDot(iX/16, iY/16);
		maze.drawDot(iX/16+(iX%16>0?1:0), iY/16+(iY%16>0?1:0));

		if (iStatus==BLIND && iBlink==1 && iBlindCount%32<16)
			graphics.drawImage(imageGhost, iX-1, iY-1, applet);
		else if (iStatus==OUT || iStatus==IN)
			graphics.drawImage(imageGhost, iX-1, iY-1, applet);
		else if (iStatus==BLIND)
			graphics.drawImage(imageBlind, iX-1, iY-1, applet);
		else 
			graphics.drawImage(imageEye, iX-1, iY-1, applet);
	}  

	public void move(int iPacX, int iPacY, int iPacDir)
	{
		if (iStatus==BLIND)
		{
			iBlindCount--;
			if (iBlindCount<blindCount/3)
				iBlink=1;
			if (iBlindCount==0)
				iStatus=OUT;
			if (iBlindCount%2==1)	// blind moves at 1/2 speed
			return;
		}

		if (! speedControl.isMove() )
			// no move
			return;

		if (iX%16==0 && iY%16==0)
			// determine direction
		{
			switch (iStatus)
			{
			case IN:
				iDir=aStar(iPacX, iPacY);
				break;
			case OUT:
				iDir=outSelect(iPacX, iPacY, iPacDir);
				break;
			case BLIND:
				iDir=blindSelect(iPacX, iPacY, iPacDir);
				break;
			case EYE:
				iDir=eyeSelect();
			}
		}

		if (iStatus!=EYE)
		{
			iX+= Resources.iXDirection[iDir];
			iY+= Resources.iYDirection[iDir];
		}
		else
		{	
			iX+=2* Resources.iXDirection[iDir];
			iY+=2* Resources.iYDirection[iDir];
		}

	}
	
	public int aStar(int iPacX, int iPacY) {
		if (!GOT_IA) return inSelect();
		if (!directions.empty()) return directions.pop();
		String[] maze_map = Resources.MazeDefine;
		int x = iX/16, y = iY/16;
		int dist[][] = new int[maze_map.length][maze_map[0].length()];
		int visited[][] = new int[maze_map.length][maze_map[0].length()];
		int parent[][][] = new int[maze_map.length][maze_map[0].length()][2];
		for (int i = 0; i < maze_map.length; i++) {
			for (int j = 0; j < maze_map[i].length(); j++) {
				visited[i][j] = 0;
				dist[i][j] = -1;
			}
		}
		dist[y][x] = 0;
		parent[y][x][0] = -1;
		parent[y][x][1] = -1;
		
		while (x != -1 && y != -1) {
			int nextX = -1, nextY = -1;
			visited[y][x] = 1;
			if (y == iPacY/16 && x == iPacX/16) break;
			for (int i = 0; i < 4; i++) {
				int nX = x + Resources.iXDirection[i];
				int nY = y + Resources.iYDirection[i];
				if (maze_map[nY].charAt(nX) == 'X') continue;
				if (visited[nY][nX] == 1) continue;
				int heuristic_value = getHeuristic(nX, nY, iPacX/16, iPacY/16);
				if (dist[nY][nX] == -1 || dist[nY][nX] > dist[y][x] + heuristic_value + 1) {
					dist[nY][nX] = 1 + dist[y][x] + heuristic_value;
					parent[nY][nX][0] = y;
					parent[nY][nX][1] = x;
					
				}
			}
			
			for (int i = 0; i < maze_map.length; i++) {
				for (int j = 0; j < maze_map[i].length(); j++) {
					if (visited[i][j] == 0 && dist[i][j] != -1) {
						if (x == -1 || dist[i][j] < dist[y][x]) {
							nextY = i;
							nextX = j;
						}
					}
				}
			}
			x = nextX; y = nextY;
		}
		
		if (visited[iPacY/16][iPacY/16] == 1) {
			x = iPacX;
			y = iPacY;
			int dir = 0;
			Stack<Integer> lol = new Stack<Integer>();
			while (parent[y][x][0] != -1) {
				int nX = x;
				int nY = y;
		
				for (dir = 0; dir < 4; dir++) {
					if (nX + Resources.iXDirection[dir] == x &&
						nY + Resources.iYDirection[dir] == y) {
						break;
					}
				}
				lol.push(dir);
			}
			for (int i = 0; i < STORE_DIRECTIONS && !lol.empty(); i++) {
				directions.push(lol.pop());
			}
			return directions.pop();
		}
		return inSelect();
	}
	
	/**
	 * count available directions
	 * @return
	 */
	public int inSelect()
	{
		int iM,i,iRand;
		int iDirTotal=0;

		for (i=0; i<4; i++)
		{
			iM=maze.iMaze[iY/16 + Resources.iYDirection[i]]
			              [iX/16 + Resources.iXDirection[i]];
			if (iM!=Maze.WALL && i != Resources.iBack[iDir] )
			{
				iDirTotal++;
			}
		}
		// randomly select a direction
		if (iDirTotal!=0)
		{
			iRand=Utilities.randSelect(iDirTotal);
			if (iRand>=iDirTotal)
				throw new Error("iRand out of range");
			//				exit(2);
			for (i=0; i<4; i++)
			{
				iM=maze.iMaze[iY/16+ Resources.iYDirection[i]]
				              [iX/16+ Resources.iXDirection[i]];
				if (iM!=Maze.WALL && i != Resources.iBack[iDir] )
				{
					iRand--;
					if (iRand<0)
						// the right selection
					{
						if (iM== Maze.DOOR)
							iStatus=OUT;
						iDir=i;	break;
					}
				}
			}
		}	
		return(iDir);	
	}

	/**
	 * count available directions
	 * @param iPacX
	 * @param iPacY
	 * @param iPacDir
	 * @return
	 */
	public int outSelect(int iPacX, int iPacY, int iPacDir)
	{
		int iM,i,iRand;
		int iDirTotal=0;
		int[] iDirCount=new int [4];

		for (i=0; i<4; i++)
		{
			iDirCount[i]=0;
			iM=maze.iMaze[iY/16 + Resources.iYDirection[i]]
			              [iX/16+ Resources.iXDirection[i]];
			if (iM!=Maze.WALL && i!= Resources.iBack[iDir] && iM!= Maze.DOOR )
				// door is not accessible for OUT
			{
				iDirCount[i]++;
				iDirCount[i]+=iDir==iPacDir?
						DIR_FACTOR:0;
				switch (i)
				{
				case 0:	// right
					iDirCount[i] += iPacX > iX ? POS_FACTOR:0;
					break;
				case 1:	// up
					iDirCount[i]+=iPacY<iY?
							POS_FACTOR:0;
					break;
				case 2:	// left
					iDirCount[i]+=iPacX<iX?
							POS_FACTOR:0;
					break;
				case 3:	// down
					iDirCount[i]+=iPacY>iY?
							POS_FACTOR:0;
					break;
				}
				iDirTotal+=iDirCount[i];
			}
		}	
		// randomly select a direction
		if (iDirTotal!=0)
		{	
			iRand=Utilities.randSelect(iDirTotal);
			if (iRand>=iDirTotal)
				throw new Error("iRand out of range");
			// exit(2);
			for (i=0; i<4; i++)
			{
				iM=maze.iMaze[iY/16+ Resources.iYDirection[i]]
				              [iX/16+ Resources.iXDirection[i]];
				if (iM!=Maze.WALL && i!= Resources.iBack[iDir] && iM!= Maze.DOOR )
				{	
					iRand-=iDirCount[i];
					if (iRand<0)
						// the right selection
					{
						iDir=i;	break;
					}
				}
			}	
		}
		else	
			throw new Error("iDirTotal out of range");
		// exit(1);
		return(iDir);
	}

	public void blind()
	{
		if (iStatus==BLIND || iStatus==OUT)
		{
			iStatus=BLIND;
			iBlindCount=blindCount;
			iBlink=0;
			// reverse
			if (iX%16!=0 || iY%16!=0)
			{
				iDir= Resources.iBack[iDir];
				// a special condition:
				// when ghost is leaving home, it can not go back
				// while becoming blind
				int iM;
				iM=maze.iMaze[iY/16+ Resources.iYDirection[iDir]]
				              [iX/16+ Resources.iXDirection[iDir]];
				if (iM == Maze.DOOR)
					iDir=Resources.iBack[iDir];
			}
		}
	}

	/**
	 * count available directions
	 * @return
	 */
	public int eyeSelect()
	{
		int iM,i,iRand;
		int iDirTotal=0;
		int [] iDirCount= new int [4];

		for (i=0; i<4; i++)
		{
			iDirCount[i]=0;
			iM=maze.iMaze[iY/16 + Resources.iYDirection[i]]
			              [iX/16+Resources.iXDirection[i]];
			if (iM!= Maze.WALL && i!= Resources.iBack[iDir])
			{
				iDirCount[i]++;
				switch (i)
				{
				// door position 10,6
				case 0:	// right
					iDirCount[i]+=160>iX?
							POS_FACTOR:0;
					break;
				case 1:	// up
					iDirCount[i]+=96<iY?
							POS_FACTOR:0;
					break;
				case 2:	// left
					iDirCount[i]+=160<iX?
							POS_FACTOR:0;
					break;
				case 3:	// down
					iDirCount[i]+=96>iY?
							POS_FACTOR:0;
					break;
				}
				iDirTotal+=iDirCount[i];
			}	
		}
		// randomly select a direction
		if (iDirTotal!=0)
		{
			iRand= Utilities.randSelect(iDirTotal);
			if (iRand>=iDirTotal)
				throw new Error("RandSelect out of range");
			//				exit(2);
			for (i=0; i<4; i++)
			{
				iM=maze.iMaze[iY/16+ Resources.iYDirection[i]]
				              [iX/16+ Resources.iXDirection[i]];
				if (iM!= Maze.WALL && i!= Resources.iBack[iDir])
				{
					iRand-=iDirCount[i];
					if (iRand<0)
						// the right selection
					{
						if (iM== Maze.DOOR)
							iStatus=IN;
						iDir=i;	break;
					}
				}
			}
		}
		else
			throw new Error("iDirTotal out of range");
		return(iDir);	
	}	

	/**
	 * count available directions
	 * @param iPacX
	 * @param iPacY
	 * @param iPacDir
	 * @return
	 */
	public int blindSelect(int iPacX, int iPacY, int iPacDir)
	{
		int iM,i,iRand;
		int iDirTotal=0;
		int [] iDirCount = new int [4];

		for (i=0; i<4; i++)
		{
			iDirCount[i]=0;
			iM=maze.iMaze[iY/16+ Resources.iYDirection[i]][iX/16+ Resources.iXDirection[i]];
			if (iM != Maze.WALL && i != Resources.iBack[iDir] && iM != Maze.DOOR)
				// door is not accessible for OUT
			{
				iDirCount[i]++;
				iDirCount[i]+=iDir==iPacDir?
						DIR_FACTOR:0;
				switch (i)
				{
				case 0:	// right
					iDirCount[i]+=iPacX<iX?
							POS_FACTOR:0;
					break;
				case 1:	// up
					iDirCount[i]+=iPacY>iY?
							POS_FACTOR:0;
					break;
				case 2:	// left
					iDirCount[i]+=iPacX>iX?
							POS_FACTOR:0;
					break;
				case 3:	// down
					iDirCount[i]+=iPacY<iY?
							POS_FACTOR:0;
					break;
				}
				iDirTotal+=iDirCount[i];
			}	
		}
		// randomly select a direction
		if (iDirTotal!=0)
		{
			iRand=Utilities.randSelect(iDirTotal);
			if (iRand>=iDirTotal)
				throw new Error("RandSelect out of range");
			//				exit(2);
			for (i=0; i<4; i++)
			{	
				iM=maze.iMaze[iY/16+ Resources.iYDirection[i]]
				              [iX/16+ Resources.iXDirection[i]];
				if (iM!= Maze.WALL && i!= Resources.iBack[iDir])
				{	
					iRand-=iDirCount[i];
					if (iRand<0)
						// the right selection
					{
						iDir=i;	break;
					}
				}
			}
		}
		else
			throw new Error("iDirTotal out of range");
		return(iDir);
	}

	/**
	 * return 1 if caught the pacman
	 * return 2 if being caught by pacman
	 * @param iPacX
	 * @param iPacY
	 * @return
	 */
	int testCollision(int iPacX, int iPacY)
	{
		if (iX<=iPacX+2 && iX>=iPacX-2
				&& iY<=iPacY+2 && iY>=iPacY-2)
		{
			switch (iStatus)
			{
			case OUT:
				return(1);
			case BLIND:
				iStatus=EYE;
				iX=iX/4*4;
				iY=iY/4*4;
				return(2);
			}	
		}
		// nothing
		return(0);
	}
	
	
}


